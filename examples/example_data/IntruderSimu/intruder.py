# -*- encoding=utf-8 -*-
from __future__ import print_function
#########################################################################################################################################################################
# Authors:
#   Raphael Maurin, raphael.maurin@imft.fr
#   Remi Chassagne, remi.chassagne@univ-grenoble-alpes.fr
# 07/02/2022
#
# Same as sedimentTransportExample but solving a 1D volume averaged fluid momentum balance to determine the fluid velocity profile (i.e. DEM-1D RANS coupling)
# The resolution therefore include a two-way coupling in time between the fluid and the particle behavior, meaning that the fluid is solved every "fluidResolPeriod"
# and account for the presence of particles and for the momentum transfered to the particle phase through the hydrodynamic forces imposed. The fluid-particle system momentum
# is therefore conserved.
#
# Data are saved in data.hdf5 file.
#
############################################################################################################################################################################

#Import libraries

from builtins import range
from yade import pack, plot
import math
import random as rand
import numpy as np
import h5py
from time import time

##
## Main parameters of the simulation
##

rand.seed(1) # PHF to fix the random bottom

#Particles
diameterPart = 5e-3 #Diameter of the particles composing the bed, in m
diameterPartI = 12.7e-3    #Diameter of the intruder, in m
numberIntruders = 1 #Number of intruders
densPart = 2500     #density of the particles, in kg/m3
phiPartMax = 0.61  #Value of the dense packing solid volume fraction, dimensionless
restitCoef = 0.5    #Restitution coefficient of the particles, dimensionless
partFrictAngle = atan(0.4)  #friction angle of the particles, in radian

#Fluid
densFluid = 1000.   #Density of the fluid, in kg/m^3
kinematicViscoFluid = 1e-6  #kinematic viscosity of the fluid, in m^2/s
waterDepth = 3.1 #Water depth, in diameter
dtFluid = 1e-5  #Time step for the fluid resolution, in s
fluidResolPeriod = 1e-2 #Time between two fluid resolution, in s

#Configuration: inclined channel
slope = 0.1 #Inclination angle of the channel slope in radian
lengthCell = 10 #Streamwise length of the periodic cell, in diameter
widthCell = 10  #Spanwise length of the periodic cell, in diameter
Nlayer = 8. #nb of layer of particle, in diameter
depthI = 3.0 #nb of layer below the top where the intruder is initially  positioned
Nlayer1 = Nlayer-depthI
Nlayer2 = depthI
print("Number of layer of background particles : {}".format(Nlayer1))
fluidHeight = (0.5+Nlayer1+Nlayer2)*diameterPart+waterDepth*diameterPart    #Height of the flow from the bottom of the sample, in m

saveData = 1    #If put to 1, at each execution of function measure() save the sediment transport rate, fluid velocity, solid volume fraction and velocity profiles for post-processing
dtSave = 1
saveVelocity = 0
paraview = 0
endTime = 50  #Time simulated (in seconds)
#Save data details
if saveData==1: #If saveData option is activated, requires a folder data
    scriptPath = os.path.abspath(os.path.dirname(sys.argv[-1])) #Path where the script is stored



##
## Secondary parameters of the simulation
##

expoDrag = 3.1  # Richardson Zaki exponent for the hindrance function of the drag force applied to the particles

#Discretization of the sample in ndimz wall-normal (z) steps of size dz, between the bottom of the channel and the position of the water free-surface. Should be equal to the length of the imposed fluid profile. Mesh used for HydroForceEngine.
dz = diameterPart/30 # Fluid discretization step in the wall-normal direction
ndimz = int(1+fluidHeight/dz) # Number of cells in the height
dz =  fluidHeight/(1.0*(ndimz-1))   #Correct the value of dz due to int() in ndimz
print("Nb cells = {}, dz={}".format(ndimz, dz))

#Geometrical configuration, define useful quantities
height = 5*fluidHeight  #heigth of the periodic cell, in m (bigger than the fluid height to take into particles jumping above the latter)
length = lengthCell*diameterPart #length of the stream, in m
width = widthCell*diameterPart  #width of the stream, in m
groundPosition = height/4.0 #Definition of the position of the ground, in m
gravityVector = Vector3(9.81*sin(slope),0.0,-9.81*cos(slope)) #Gravity vector to consider a channel inclined with slope angle 'slope'

#Particles contact law/material parameters
maxPressure = (densPart-densFluid)*phiPartMax*(Nlayer*diameterPart)*abs(gravityVector[2]) #Estimated max particle pressure from the static load
normalStiffness = maxPressure*diameterPart*1e4 #Evaluate the minimal normal stiffness to be in the rigid particle limit (cf Roux and Combe 2002)
youngMod1 = normalStiffness/diameterPart    #Young modulus of the particles from the stiffness wanted.
youngMod2 = normalStiffness/diameterPartI   #Young modulus of the particles from the stiffness wanted.
poissonRatio = 0.5  #poisson's ratio of the particles. Classical values, does not have much influence
O.materials.append(ViscElMat(en=restitCoef, et=0., young=youngMod1, poisson=poissonRatio, density=densPart, frictionAngle=partFrictAngle, label='Mat1'))  
O.materials.append(ViscElMat(en=restitCoef, et=0., young=youngMod2, poisson=poissonRatio, density=densPart, frictionAngle=partFrictAngle, label='Mat2'))

########################
## FRAMEWORK CREATION ##
########################

#Definition of the semi-periodic cell
O.periodic = True
O.cell.setBox(length,width,height)

# Reference walls: build two planes at the ground and free-surface to have a reference for the eyes in the 3D view
lowPlane = box(
        center= (length/2.0, width/2.0,groundPosition),
        extents=(200,200,0),
        fixed=True,
        wire=False,
        color = (0.,1.,0.),
        material = 'Mat1')

WaterSurface = box(
        center= (length/2.0, width/2.0,groundPosition+fluidHeight),
        extents=(2000,width/2.0,0),
        fixed=True,wire=False,
        color = (0,0,1),
        material = 'Mat1',mask = 0)
O.bodies.append([lowPlane,WaterSurface]) #add to simulation

# Regular arrangement of spheres sticked at the bottom with random height
L = range(0,int(length/(diameterPart))) #The length is divided in particle diameter
W = range(0,int(width/(diameterPart))) #The width is divided in particle diameter

for x in L: #loop creating a set of sphere sticked at the bottom with a (uniform) random altitude comprised between 0.5 (diameter/12) and 5.5mm (11diameter/12) with steps of 0.5mm. The repartition along z is made around groundPosition.
    for y in W:
        n =  rand.randrange(0,12,1)/12.0*diameterPart     #Define a number between 0 and 11/12 diameter with steps of 1/12 diameter (0.5mm in the experiment)
        O.bodies.append(sphere((x*diameterPart, y*diameterPart,groundPosition - 11*diameterPart/12.0/2.0 + n),diameterPart/2.,color=(0,0,0),fixed = True,material = 'Mat1'))


#Create a loose cloud of particle inside the cell
partCloud1 = pack.SpherePack()
partCloud2 = pack.SpherePack()
partCloud3 = pack.SpherePack()
partVolume = pi/6.*pow(diameterPart,3) #Volume of a particle 1
partVolumeI = pi/6.*pow(diameterPartI,3) #Volume of a particle 2
partNumber1 = int(Nlayer1*phiPartMax*diameterPart*length*width/partVolume) #Volume of beads to obtain Nlayer1 layers of particles 1
partNumber2 = numberIntruders #Number of intruders
partNumber3 = int(Nlayer2*phiPartMax*diameterPart*length*width/partVolume) #Volume of beads to obtain Nlayer2 layers of particles 2
#Define the deposition height considering that the packing realised by make cloud is 0.2
depositionHeight1 = max(Nlayer1*phiPartMax/0.2*diameterPart,diameterPart) #Consider that the packing realised by make cloud is 0.2
depositionHeight2 = 1.2 * diameterPartI
depositionHeight3 = max(Nlayer2*phiPartMax/0.2*diameterPart,diameterPart) #Consider that the packing realised by make cloud is 0.2
partCloud1.makeCloud(minCorner=(0,0.,groundPosition+diameterPart+1e-4),maxCorner=(length,width,groundPosition+depositionHeight1),
                     rRelFuzz=0., rMean=diameterPart/2.0, num = partNumber1,seed=9) #PHF seed
partCloud2.makeCloud(minCorner=(0,0.,groundPosition+depositionHeight1),
maxCorner=(length,width,groundPosition+depositionHeight1+depositionHeight2),
                     rRelFuzz=0., rMean=diameterPartI/2.0, num = partNumber2,seed=1) #PHF seed
partCloud3.makeCloud(minCorner=(0,0.,groundPosition+depositionHeight1+depositionHeight2),maxCorner=(length,width,groundPosition+depositionHeight1+depositionHeight2+depositionHeight3),
                     rRelFuzz=0., rMean=diameterPart/2.0, num = partNumber3,seed=9) #PHF seed
partCloud1.toSimulation(material='Mat1', color=(0,0,1)) #Send this packing to simulation with material Mat
partCloud2.toSimulation(material='Mat2', color=(1,0,0)) #Send this packing to simulation with material Mat
partCloud3.toSimulation(material='Mat1', color=(0,0,1))
#Evaluate the deposition time considering the free-fall time of the highest particle to the ground
depoTime = 2*sqrt(fluidHeight*2/abs(gravityVector[2]))

# Collect the ids of the spheres which are dynamic to add a fluid force through HydroForceEngines
idApplyForce = []
idIntruders = []
for b in O.bodies: 
    if isinstance(b.shape,Sphere) and b.dynamic:
        idApplyForce+=[b.id]
        if b.shape.radius == diameterPartI/2:
            idIntruders.append(b.id)


#########################
#### SIMULATION LOOP#####
#########################

O.engines = [
    # Reset the forces
    ForceResetter(),
    # Detect the potential contacts
    InsertionSortCollider([Bo1_Sphere_Aabb(), Bo1_Wall_Aabb(),Bo1_Facet_Aabb(),Bo1_Box_Aabb()],label='contactDetection',allowBiggerThanPeriod = True),
    # Calculate the different interactions
    InteractionLoop(
        [Ig2_Sphere_Sphere_ScGeom(), Ig2_Box_Sphere_ScGeom()],
        [Ip2_ViscElMat_ViscElMat_ViscElPhys()],
        [Law2_ScGeom_ViscElPhys_Basic()],
        label = 'interactionLoop'),
    #Apply an hydrodynamic force to the particles
    HydroForceEngine(
        densFluid = densFluid,
        viscoDyn = kinematicViscoFluid*densFluid,
        zRef = groundPosition,
        gravity = gravityVector,
        deltaZ = dz,
        expoRZ = expoDrag,
        lift = False,
        nCell = ndimz,
        vCell = length*width*dz,
        ids = idApplyForce,
        label = 'hydroEngine',
        phiMax=phiPartMax,
        dead = True),
    #Solve the fluid volume-averaged 1D momentum balance, RANS 1D
    PyRunner(command = 'fluidModel()', virtPeriod = fluidResolPeriod, label = 'fluidRes', dead = True),
    #Measurement, output files
    PyRunner(command = 'measure()', virtPeriod = dtSave, label = 'measurement', dead = True),
    # Check if the packing is stabilized, if yes activate the hydro force on the grains and the slope.
    PyRunner(command='gravityDeposition(depoTime)',virtPeriod = 0.01,label = 'gravDepo'),
    #GlobalStiffnessTimeStepper, determine the time step
    GlobalStiffnessTimeStepper(defaultDt = 1e-4, viscEl = True,timestepSafetyCoefficient = 0.7,  label = 'GSTS'),
    # Integrate the equation and calculate the new position/velocities...
    NewtonIntegrator(damping=0.2, gravity=gravityVector, label='newtonIntegr')
    ]

if paraview:
    print("video activated : register vtk file for use of paraview")
    O.engines += [VTKRecorder(virtPeriod=1.,recorders=['spheres','colors'],fileName=scriptPath+'/Paraview/p1-')]


t1 = time()
#save the initial configuration to be able to recharge the simulation starting configuration easily
O.saveTmp()
#Initialize HydroForceEngine variables to zero (fluid velocity, fluctuations,...)
hydroEngine.initialization()
hydroEngine.enableMultiClassAverage = True  #Activate the averaging over different class/size of particles
#run
O.run()

####################################################################################################################################
####################################################  FUNCTION DEFINITION  #########################################################
####################################################################################################################################



######                                         ######
### LET THE TIME FOR THE GRAVITY DEPOSITION AND ACTIVATE THE FLUID AT THE END ###
######                                         ######
def gravityDeposition(lim):
    if O.time<lim : 
        return
    else :
        print('\n Gravity deposition finished, apply fluid forces !\n')
        newtonIntegr.damping = 0.0  # Set the artificial numerical damping to zero
        gravDepo.dead = True    # Remove the present engine for the following
        hydroEngine.dead = False    # Activate the HydroForceEngine
        hydroEngine.ReynoldStresses = np.zeros(ndimz) # Send the ( PHF not simplified) fluid Reynolds stresses Rxz/\rho^f used to account for the fluid velocity fluctuations in HydroForceEngine (see c++ code) PHF here initialized to zero
# PHF   hydroEngine.ReynoldStresses = np.ones(ndimz)*1e-4 # PHF test effect of initialization to non zero : NOT REPRODUCIBLE
        hydroEngine.turbulentFluctuation() #Initialize the fluid velocity fluctuation associated to particles to zero in HydroForceEngine, necessary to avoid segmentation fault
        measurement.dead = False    # Activate the measure() PyRunner
        fluidRes.dead = False       # Activate the 1D fluid resolution
        hydroEngine.averageProfile()    #Evaluate the solid volume fraction, velocity and drag, necessary for the fluid resolution. 
        hydroEngine.fluidResolution(1.,dtFluid) #Initialize the fluid resolution, run the fluid resolution for 1s
    return
###############
#########################################


#######               ########
###     FLUID RESOLUTION       ###
#######               ########
def fluidModel():
    #Evaluate the average vx,vy,vz,phi,drag profiles and store it in hydroEngine, to prepare the fluid resolution
    hydroEngine.averageProfile()
    #Fluid resolution
    hydroEngine.fluidResolution(fluidResolPeriod,dtFluid)   #Solve the fluid momentum balance for a time of fluidResolPeriod s with a time step dtFluid
    #update the fluid velocity for later save
    vxFluid = np.array(hydroEngine.vxFluid)


#######           ########
###     OUTPUT     ###
#######           ########
#Initialization
qsMean = 0      #Mean dimensionless sediment transport rate
fileNumber = 0
zAxis = np.zeros(ndimz) #z scale, in diameter
for i in range(0,ndimz):#z scale used for the possible plot at the end
    zAxis[i] = i*dz/diameterPart

#Save general Parameters:
if saveData == 1:
    parameters = ['diameterPart', 'diameterPartI', 'fluidHeight', 'waterDepth', 'Nlayer', 'zAxis', 'depthI', 'densPart', 'densFluid']
    nameFile = scriptPath + '/data.hdf5'
    h5File = h5py.File(nameFile, 'a')
    grp = h5File.create_group('generalParameters')
    for varName in parameters:
        dataSet = grp.create_dataset(name=varName, data=globals()[varName])
    h5File.close()

# Averaging/Save
def measure():
    global currentTime, qsMean, vPart, phiPart, vxFluid, Rxz, contactStresses, fileNumber, turbulentViscosity
    global positionIntru, velocityIntru
    #Evaluate the average depth profile of streamwise, spanwise and wall-normal particle velocity, particle volume fraction (and drag force for coupling with RANS fluid resolution), and store it in hydroEngine variables vxPart, phiPart, vyPart, vzPart, averageDrag.
    hydroEngine.averageProfile()
    #Extract the calculated vector. They can be saved and plotted afterwards. 
    vPart = np.array(hydroEngine.vPart)
    phiPart = np.array(hydroEngine.phiPart)

    #Extract granular stresses originating from contacts only
    contactStresses = np.array(getStressProfile_contact(
            volume=width*length*dz, nCell=ndimz,
            dz=dz, zRef=groundPosition)[0])
    contactStresses.resize(ndimz, 9) #resize in a more convenience form

    #Extract fluid velocity and put it on the same grid as particle velocity
    vxFluid_temp = np.array(hydroEngine.vxFluid)
    vxFluid = np.zeros(ndimz)
    vxFluid[0] = vxFluid_temp[0]
    vxFluid[-1] = vxFluid_temp[-1]
    for i in range(1, ndimz-1):
        vxFluid[i] = (vxFluid_temp[i]+vxFluid_temp[i+1])/2

    turbulentViscosity = np.array(hydroEngine.turbulentViscosity)
    Rxz = np.array(hydroEngine.ReynoldStresses)

    #Extract intruders position and velocity
    positionIntru = np.zeros((numberIntruders, 3))
    velocityIntru = np.zeros((numberIntruders, 3))
    for i in range(numberIntruders):
        b = O.bodies[idIntruders[i]]
        positionIntru[i] = b.state.pos
        velocityIntru[i] = b.state.vel

    #Evaluate the dimensionless sediment transport rate for information
    qsMean = sum(phiPart * vPart[:,0]) * dz / sqrt((densPart / densFluid - 1) * abs(gravityVector[2]) * pow(diameterPart, 3))
    plot.addData(SedimentRate=qsMean, time=O.time)  #Plot it during the simulation

    #Condition to stop the simulation after endTime seconds
    if O.time>=endTime:
        print('\n End of the simulation, simulated {0}s as required !\n '.format(endTime))
        O.pause()

    #Evaluate the Shields number from the maximum of the Reynolds stresses evaluated in the fluid resolution
    shieldsNumber = max(hydroEngine.ReynoldStresses)/((densPart-densFluid)*diameterPart*abs(gravityVector[2]))
    #print ("Shields number : {}".format(shieldsNumber))
    print('t={}, Sh={}, qs={} \n'.format(O.time,shieldsNumber, qsMean)) #PHF

    currentTime = O.time

    if saveData==1: #Save data for postprocessing
        nameFile = scriptPath + '/data.hdf5'    # Name of the file that will be saved
        globalParam =  ['currentTime', 'qsMean','phiPart', 'vPart', 'vxFluid', 'contactStresses', 'turbulentViscosity', 'Rxz']    # Variables to save
        globalParam += ['positionIntru', 'velocityIntru']
        Save(nameFile, globalParam) #Save

    fileNumber+=1   #Increment the file number
#Plot the dimensionless sediment transport rate as a function of time during the simulation
plot.plots={'time':('SedimentRate')}
plot.plot()

################
##########################################


#Function to save global variables in a python file which can be re-executed for post-processing
def Save(filePathName, globalVariables):
    h5File = h5py.File(filePathName, 'a')
    grp = h5File.create_group(str(globals()['fileNumber']))
    for varName in globalVariables:
        dataSet = grp.create_dataset(name=varName, data=globals()[varName])
    h5File.close()
