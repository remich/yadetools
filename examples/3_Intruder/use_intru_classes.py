"""
Use HydroFull and HydroMean to post-process YADE simulation
===========================================================
This example shows how to organize the results of a YADE simulation
into a HydroFull object, manage data, compute average quantities,
save them in another file and read average data with HydroMean class.
"""

# .. note:: IntruFull and IntruMean classes are dadicated to YADE simulations
#           performed with HydroForceEngine module in which one or several
#           intruder particles have been added.
#
# .. note:: Simulation time data have to be stored in a hdf5 file (default
#           data.hdf5). Each time step correspond to a grp of the hdf5 file
#           whose name is the number of time step. The yade script is supplied
#           to indicate how data have to be saved.


####################################################
# First create a simulation object with IntruFull
# --------------------------------------------------
#
# .. note:: This class allows you to create an object associated to a
#           simulation
#

# import the class HydroFull
from yadetools import IntruFull

#path were all simulations are located
path = '../example_data/'
#Name of the simulation to load, if not given, the program lists all simulations
#located in path and ask you to choose which one to load
simu = 'IntruderSimu'

#Load simulation and create an object called mySimu that contain the results of
#the simulation. By default all time steps (50 for this example) are loaded.
mySimu = IntruFull(simu=simu, path=path)
# .. note:: Other arguments can be used. Type HydroFull? in a ipython shell
#           to see all arguments.

mySimu.keys()
# .. note:: function keys() indicates that in addition to hydroclasses you have
#           access to positionIntru and velocityIntru corresponding to the
#           position and velocity vector at each time and each intruder.

mySimu.data[20].var["positionIntru"]

# ..note:: function manage_intruder_data() reorganizes the intruder data to make
#          them more acessible. This function is automatically runned when
#          creating the mySimu object. It allows you to have time series of the
#          position and velocity of each intruder as mySimu.posIntru and
#          mySimu.velIntru

mySimu.posIntru
mySimu.velIntru

##########################################
# Plot the position of the intruder
# ----------------------------------------
#
# .. note:: You can plot the position as a function of time of the intruder
#           and its trajectory.

import matplotlib.pyplot as plt
plt.ion()

plt.figure()
plt.plot(mySimu.time, mySimu.posIntru[2])
plt.grid()
plt.legend()
plt.xlabel(r'$t$ (s)', fontsize=20)
plt.ylabel(r'$z$ (m)', fontsize=20)

plt.figure()
plt.plot(mySimu.posIntru[2], mySimu.posIntru[0])
plt.grid()
plt.legend()
plt.ylabel(r'$z$ (m)', fontsize=20)

##########################################
# Save average quantities
# ----------------------------------------
#
# .. note:: When dealing with long simulations who might prefer not to load time
#           data each time. You can save the average quantities in an average.hdf5
#           file that will be located in the simulation folder. This is done with function:
#           mySimu.write_data("vPartMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("zAxis", file_name='average.hdf5', overwrite=1)
mySimu.write_data("phiPartMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("epsilonMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("vPartMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("vxFluidMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("contactStressesMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("RxzMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("posIntru", file_name='average.hdf5', overwrite=1)
mySimu.write_data("velIntru", file_name='average.hdf5', overwrite=1)

# .. note:: Function mySimu.save_average_fields allows you to make these 7
#           instructions in one.
mySimu.save_average_fields(file_name='average.hdf5', overwrite=1)

##########################################
# Loading average fields only
# ----------------------------------------
#
# .. note:: Once you have created file average.hdf5 and saved averaged fields,
#           you might want to load only average fields. This is done with class
#           HydroMean

from yadetools import IntruMean
mySimuAverage = IntruMean(simu=simu, path=path)
# .. note:: Other arguments can be used. Type HydroMean? in a ipython shell
#           to see all arguments.

mySimuAverage.keys()
# .. note:: function keys() indicates that variables named 'RxzMean', 'contactStressesMean',
#           'phiPartMean', 'vPartMean', 'vxFluidMean', 'zAxis', 'posIntru' and 'velIntru' have
#           been loaded. You can access the time data through mySimuAverage.vPartMean for the 
#           favre average particle velocity.

# .. note:: you can plot these felds directly as previously with class HydroFull
plt.figure()
plt.plot(mySimuAverage.time, mySimuAverage.posIntru[2])
plt.grid()
plt.legend()
plt.xlabel(r'$t$ (s)', fontsize=20)
plt.ylabel(r'$z$ (m)', fontsize=20)

plt.figure()
plt.plot(mySimuAverage.posIntru[2], mySimuAverage.posIntru[0])
plt.grid()
plt.legend()
plt.ylabel(r'$z$ (m)', fontsize=20)

