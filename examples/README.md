# yadetools example

This directory provides usefull example showing how to use yadetools classes.

# How to use

Each example contains a python file with lots of comments explaining how to use
the classes. Open it with a text editor to read it and execute it in a ipython
shell.
