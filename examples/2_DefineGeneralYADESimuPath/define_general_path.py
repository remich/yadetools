"""
Define a general path for your simulation location.
===================================================
This example shows how to define a general path containing all your YADE
simulations. If all your simulations are located in a fixed directory (with
as many sub-directories as you want), this provides a very convient way to work
interactively with your data.
"""


####################################################
# First define the general path of simulation
# --------------------------------------------------
#
# .. note:: This is done with function define_yade_simu_path(). This creates
#           .env in your installation directory containing the path of your
#           YADE simulations, which is used an envrinment variable.

# import
from yadetools import define_yade_simu_path

# .. note:: You just have to follow instructions. Warning: the path has to be
# given from the root (i.e. /home/user/path and no ~/path/ nor ../path/)
define_yade_simu_path()

# .. note:: You can modify this general path calling function
#           define_yade_simu_path again.

####################################################
# How to use it
# --------------------------------------------------
#
# .. note:: This is very simple. Now, when you define yadetools object (like
#           HydroFull or HydroMean), you don't have to specify a path anymore.
#           You can provide the name of the simulation (i.e. name of the simulation
#           folder) or not. In the later case, all simulations contained in the
#           general path of simulations just defined (i.e. every folder
#           containing a data.hdf5 file) are listed and you are asked to choose
#           the one you want.

from yadetools import HydroFull
mySimu = HydroFull()

# .. note:: If you want to load a simulation not located in the general path of
#           YADE simulations, you just have to provide a path argument when you
#           define the object. For example you can still load the example data.
exampleSimu = HydroFull(simu='SedTransportSimu', path='../example_data/')
