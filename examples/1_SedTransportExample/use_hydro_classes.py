"""
Use HydroFull and HydroMean to post-process YADE simulation
===========================================================
This example shows how to organize the results of a YADE simulation
into a HydroFull object, manage data, compute average quantities,
save them in another file and read average data with HydroMean class.
"""

# .. note:: HydroFull and HydroMean classes are dadicated to YADE simulations
#           performed with HydroForceEngine module.
#
# .. note:: Simulation time data have to be stored in a hdf5 file (default
#           data.hdf5). Each time step correspond to a grp of the hdf5 file
#           whose name is the number of time step. The yade script is supplied
#           to indicate how data have to be saved.


####################################################
# First create a simulation object with HydroFull
# --------------------------------------------------
#
# .. note:: This class allows you to create an object associated to a
#           simulation
#

# import the class HydroFull
from yadetools import HydroFull

#path were all simulations are located
path = '../example_data/'
#Name of the simulation to load, if not given, the program lists all simulations
#located in path and ask you to choose which one to load
simu = 'SedTransportSimu'

#Load simulation and create an object called mySimu that contain the results of
#the simulation. By default all time steps (50 for this example) are loaded.
mySimu = HydroFull(simu=simu, path=path)
# .. note:: Other arguments can be used. Type HydroFull? in a ipython shell
#           to see all arguments.

mySimu.keys()
# .. note:: function keys() indicates that variables named 'Rxz', 'contactStresses',
#           'phiPart', 'vPart', 'vxFluid' and 'zAxis' have been loaded. You can
#           access the time data through mySimu.data[20].var["vPart"] for the particle 
#           velocity field at time step 20 for example.

mySimu.data[20].var["vPart"]

##########################################
# Computation of favre average field
# ----------------------------------------
#
# .. note:: You might want to compute average field using a favre average
#           procedure. You can do it with function:
#           mySimu.favre_average(volume_fraction='phiPart', quantity='vPart')
#           With the first argument you indicate the phase over which the favre
#           average is performed (in this case the granular phase) and the second
#           argument indicates the field to average (in this case particle
#           velocity).

mySimu.favre_average('phiPart', 'vPart')
mySimu.favre_average('phiPart', 'contactStresses')
mySimu.favre_average('epsilon', 'vxFluid')
mySimu.favre_average('epsilon', 'Rxz')

# .. note:: Function mySimu.average_all() allows you to make these 4 averages in
#           one line only.
mySimu.average_all()

# .. note:: Each averaged field is saved as variable of class mySimu whose name
#           is suffixed with Mean. For example mySimu.vPartMean. You can plot
#           averaged field with matplotlib.
import matplotlib.pyplot as plt
plt.ion()

plt.figure()
plt.plot(mySimu.vPartMean[0], mySimu.zAxis, label=r'$\left<v_x\right>^p$')
plt.plot(mySimu.vxFluidMean, mySimu.zAxis, label=r'$\left<v_x\right>^f$')
plt.grid()
plt.legend()
plt.xlabel(r'(m/s)', fontsize=20)
plt.ylabel(r'$z$ (m)', fontsize=20)

plt.figure()
plt.plot(mySimu.phiPartMean, mySimu.zAxis, label=r'$\phi$')
plt.plot(mySimu.epsilonMean, mySimu.zAxis, label=r'$\epsilon$')
plt.grid()
plt.legend()
plt.ylabel(r'$z$ (m)', fontsize=20)

##########################################
# Save average quantities
# ----------------------------------------
#
# .. note:: When dealing with long simulations who might prefer not to load time
#           data each time. You can save the average quantities in an average.hdf5
#           file that will be located in the simulation folder. This is done with function:
#           mySimu.write_data("vPartMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("zAxis", file_name='average.hdf5', overwrite=1)
mySimu.write_data("phiPartMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("epsilonMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("vPartMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("vxFluidMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("contactStressesMean", file_name='average.hdf5', overwrite=1)
mySimu.write_data("RxzMean", file_name='average.hdf5', overwrite=1)

# .. note:: Function mySimu.save_average_fields allows you to make these 7
#           instructions in one.
mySimu.save_average_fields(file_name='average.hdf5', overwrite=1)

##########################################
# Loading average fields only
# ----------------------------------------
#
# .. note:: Once you have created file average.hdf5 and saved averaged fields,
#           you might want to load only average fields. This is done with class
#           HydroMean

from yadetools import HydroMean
mySimuAverage = HydroMean(simu=simu, path=path)
# .. note:: Other arguments can be used. Type HydroMean? in a ipython shell
#           to see all arguments.

mySimuAverage.keys()
# .. note:: function keys() indicates that variables named 'RxzMean', 'contactStressesMean',
#           'phiPartMean', 'vPartMean', 'vxFluidMean' and 'zAxis' have been loaded. You can
#           access the time data through mySimuAverage.vPartMean for the favre average
#           particle velocity.

# .. note:: you can plot these felds directly as previously with class HydroFull
plt.figure()
plt.plot(-mySimuAverage.contactStressesMean[8], mySimuAverage.zAxis, label=r'$-\left<\tau_{zz}\right>^p$')
plt.plot(mySimuAverage.contactStressesMean[2], mySimuAverage.zAxis, label=r'$\left<\tau_{xz}\right>^p$')
plt.plot(mySimuAverage.RxzMean, mySimuAverage.zAxis, label=r'$\left<R_{xz}\right>^f$')
plt.grid()
plt.legend()
plt.xlabel(r'(Pa)', fontsize=20)
plt.ylabel(r'$z$ (m)', fontsize=20)

