"""Class to load all data saved at timeStep of an openFoam simulation
=====================================================================

.. autoclass:: IntruFull
.. autoclass:: IntruMean

"""
from yadetools.hydrosimu import HydroFull, HydroMean
import numpy as np


class IntruFull(HydroFull):
    """
    Load time data of sediment transport YADE simulation with one or several
    intruder of different size.
    Data must be stored in hdf5 format.

    Args:
        simu : str, name of folder containing the desired simulation.
            If None, look for all availables simulations in path.
        path : str, path in which to look for simulation simu.
            If None, consider the current and sub-directories.
        hdf5_file : str, name of the hdf5 file to load
        start : int, first time step to load
        end : int, last time step to load. If None, load the last
            available time step in hdf5_file
        step : int, step in between each loaded time step. If None,
            load all time step from start to end.
        data_to_load : list of str, names of the variables to load.
            If None, load all available variables.
        start_average : int, first time step to include in the average.
            If None, use transTime/dtSave if those attributes are present,
            otherwise the first loaded time step.
        end_average : int, last time step to include in the average.
            If None, use the last loaded time step.
        verbose: bool, set to False to remove loading information.
            will still show warnings and errors.
    """

    def __init__(self, simu=None, path=None, hdf5_file='data.hdf5',
            start=0, end=None, step=None, data_to_load=None, 
            start_average=None, end_average=None, verbose=True):
        HydroFull.__init__(self, simu, path, hdf5_file, start, end,
                           step, data_to_load, start_average, end_average, 
                           verbose=verbose)
        self.manage_intruder_data(verbose=verbose)

    def manage_intruder_data(self, verbose=True):
        '''
        Load time-series from intruder data. 

        Output format : for each time step i, intruder k :
            quantityIntru[k,i] (or quantityIntru[k,...,i] for vectors or 
                                multidimension matrix)
            
        Input format : for each time step i, intruder k, contact n :
            - old format (one array for all the intruders): 
                data[i].var['quantityIntru'][k]
            - new format (one dictionary for one intruder / contact):
                data[i].var['intru_k']['quantity']
                data[i].var['intru_k']['contact_n']['quantity']
                
        New imput format is compatible with data loaded from hdf5 groups 
        which are more suitable to store the contact interactions data : 
            1. it is more readable (we don't work with 3 dimensions matrix),
            2. it avoids to deal with different numbers of contact for each
            intruder : means working with arrays of arrays of different sizes
            which is not compatible with hdf5 datasets.   
        '''
        
        try:  # OLD INPUT FORMAT            
            keys = [key for key in self.loaded_time_variables 
                    if 'Intru' in key]
            self.nbIntru = len(self.data[0].var[keys[0]])
            for key in keys:
                dim = len(self.data[0].var[key][0])
                setattr(self, key, np.empty((self.nbIntru, dim, len(self.data))))
                for i, data in enumerate(self.data):
                   getattr(self, key)[:,:,i] = data.var[key][:,:]
                self.time_variables.append(key)
        
        except IndexError:  # NEW INPUT FORMAT
            intr_names = [key for key in self.loaded_time_variables 
                           if 'intru_' in key]
            if not intr_names:
                if verbose:
                    print("No intruder data to manage.")
                return
            intr_num = [int(name[6:]) for name in intr_names]
            self.nbIntru = len(intr_names)
            
            # deals with intruder quantities (e.g. 'pos', 'vel', 'force', 'torque'...) 
            # whatever the data dimension for example a 3 dim matrix (q,p,r), 
            # data of name 'key' will be stored in an array `keyIntru` and accessed
            # like keyIntru[k,q,p,r,i] with k the number of intruder and i the time step:
            keys = [key for key, value in self.data[0].var[intr_names[0]].items() 
                    if isinstance(value, np.ndarray)]
            for key in keys:
                attr_name = key + 'Intru'
                key_shape = self.data[0].var[intr_names[0]][key].shape
                attr_shape = (self.nbIntru,) + key_shape + (len(self.data),)
                setattr(self, attr_name, np.empty(attr_shape))
                for i, data in enumerate(self.data):
                    for k, name in zip(intr_num, intr_names):
                        index = (k,) + (slice(None),) * len(key_shape) + (i,)
                        getattr(self, attr_name)[index] = data.var[name][key][...]
                self.time_variables.append(attr_name)
                
            # DEPRECATED. Deals with intruder contact interactions.
            # The recommended method is to manage the contact directly for each 
            # iteration in Yade and directly store the averaged quantities of 
            # interest in the hdf5 (e.g. nbContact, forceContact...). Doing the 
            # computation at each iteration allows to retrieve true mean values 
            # and have smoothier signals. It is then not useful anymore to store 
            # the contact in the hdf5 and manage them afterward. 
            # We keep this function for compatibility with old data OR for explicitely
            # stored instantenous data (must have averageIntrData attribute false).
            if hasattr(self, "averageIntrData") and not self.averageIntrData:
                self.nbContactIntru = np.empty((self.nbIntru, len(self.data)))
                self.contactForceIntru = np.zeros((self.nbIntru, 3, len(self.data)))
                self.contactTorqueIntru = np.zeros((self.nbIntru, 3, len(self.data)))
                for i, data in enumerate(self.data):
                    for k, intr_name in zip(intr_num, intr_names): 
                        contact_names = [key for key in data.var[intr_name].keys() 
                                         if 'contact_' in key]
                        self.nbContactIntru[k,i] = len(contact_names)
                        pos_intru = data.var[intr_name]['pos']
                        for contact_name in contact_names:
                            shear_force = data.var[intr_name][contact_name]['shearForce']
                            normal_force = data.var[intr_name][contact_name]['normalForce']
                            pos_contact = data.var[intr_name][contact_name]['pos']
                            self.contactForceIntru[k,:,i] += shear_force + normal_force
                            self.contactTorqueIntru[k,:,i] += np.cross(pos_contact - pos_intru, shear_force)   
                for key in ["nbContactIntru", "contactForceIntru", "contactTorqueIntru"]:
                    self.time_variables.append(key)

class IntruMean(HydroMean):
    """
    Load time average data of sediment transport YADE simulation.
    Data must be stored in hdf5 format.

    Args:
        simu : str, name of folder containing the desired simulation.
            If None, look for all availables simulations in path.
        path : str, path in which to look for simulation simu.
            If None, consider the current and sub-directories.
        hdf5_file : str, name of the hdf5 file to load
        data_to_load : list of str, names of the variables to load.
            If None, load all available variables.
        verbose: bool, set to False to remove loading information.
            will still show warnings and errors.
    """

    def __init__(self, simu=None, path=None, hdf5_file='average.hdf5',
            data_to_load=None, verbose=True):
        HydroMean.__init__(self, simu, path, hdf5_file, data_to_load, 
                           verbose=verbose)
