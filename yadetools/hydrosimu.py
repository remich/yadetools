"""Class to load all data saved at timeStep of an openFoam simulation
=====================================================================

.. autoclass:: HydroFull
.. autoclass:: HydroMean

"""
from yadetools.yadesimu import YadeFull, YadeMean
from yadetools._utils import derivate, integrate, primitive
import numpy as np
import matplotlib.pyplot as plt
plt.ion()


class HydroFull(YadeFull):
    """
    Load time data of sediment transport YADE simulation.
    Data must be stored in hdf5 format.

    Args:
        simu : str, name of folder containing the desired simulation.
            If None, look for all availables simulations in path.
        path : str, path in which to look for simulation simu.
            If None, consider the current and sub-directories.
        hdf5_file : str, name of the hdf5 file to load
        start : int, first time step to load
        end : int, last time step to load. If None, load the last
            available time step in hdf5_file
        step : int, step in between each loaded time step. If None,
            load all time step from start to end.
        data_to_load : list of str, names of the variables to load.
            If None, load all available variables.
        start_average : int, first time step to include in the average.
            If None, use transTime/dtSave if those attributes are present,
            otherwise the first loaded time step.
        end_average : int, last time step to include in the average.
            If None, use the last loaded time step.
        verbose: bool, set to False to remove loading information.
            will still show warnings and errors.
    """

    def __init__(self, simu=None, path=None, hdf5_file='data.hdf5',
            start=0, end=None, step=None, data_to_load=None, 
            start_average=None, end_average=None, verbose=True):
        YadeFull.__init__(self, simu, path, hdf5_file, start, end,
                step, data_to_load, verbose=verbose)
        self.time_series()
        self.average_all(start_average, end_average, verbose=verbose)

    def time_series(self, variables={}):
        """
        Create arrays for a better access to time series.
        
        Args:
            variables: dict, set supplementary variables to add to time series,
                keys are the names of the variables from loaded_time_variable,
                values are the new names to give to the time series.
        """
        default_variables = {'currentTime':'time',
                             'einstein':'einstein',
                             'ql':'qw',
                             'qs':'qs',
                             'qsMean':'qs',
                             'qw':'qw',
                             'shields':'shields',
                             'simuTime':'simuTime',}
        variables = {**default_variables, **variables}
        self.time_variables = []
        for data_name, new_name in variables.items():
            if data_name in self.loaded_time_variables:
                new_var = np.array([data.var[data_name] for data in self.data])
                setattr(self, new_name, new_var)
                self.time_variables.append(new_name)

    def average_all(self, start_average=None, end_average=None, verbose=True):
        """
        Compute the favre average of all loaded quantities.
        """
        for var in self.loaded_time_variables:
            if 'phi' not in var and 'epsilon' not in var:
                if 'Fluid' in var or var in ['Rxz']:
                    phase = 'fluid'
                    volume_fraction = 'epsilon'
                elif 'Part' in var or var in ['contactStresses', 
                                              'turbulentViscosity',]:
                    phase = 'granular'
                    volume_fraction = 'phiPart'
                    if 'Part' in var:
                        # find the phiPart suffix in case var is computed in a 
                        # subdomain. 'Part' must be written before the suffix 
                        # (e.g. var='vPartWall' -> volume_fraction='phiPartWall')
                        volume_fraction += var[var.find('Part')+4::]
                else:
                    continue
                self.favre_average(volume_fraction, var, start_average, end_average)
                if verbose:
                    print(f'Variable {var} has been favre-averaged with respect '
                          f'to the {phase} phase')

    def favre_average(self, volume_fraction, quantity, 
                      start_average=None, end_average=None):
        """
        Compute the favre average of "quantity" as a function of the
        specified volume fraction (e.g. "phiPart" for a particle quantity or
        "epsilon" for a fluid quantity). The averaged quantity is
        storred as a variable with the same name as quantity suffixed
        by "Mean".

        Args:
            volume_fraction : str, for example 'phiPart' or 'epsilon'  
            quantity : str, name of the variable to average. For ex. "vPart".
            start_average : int, first time step to include in the average.
                Default is transTime/dtSave if those attributes are present.
            end_average : int, last time step to include in the average.
        """
        
        #get data for which time_step is within start_average and end_average
        if start_average is None:
            try:
                start_average = int(self.transTime / self.dtSave)
            except AttributeError:
                pass
        data = [d for d in self.data 
                if (start_average is None or d.time_step >= start_average) 
                and (end_average is None or d.time_step <= end_average)]
        
        #Verify if quantity and volume fraction exist
        try :
            data[0].var[quantity]
        except KeyError:
            print('Quantity {} not available. Average procedure skipped.'.format(quantity))
            return
        try :
            data[0].var[volume_fraction]
        except KeyError:
            if volume_fraction == 'epsilon':
                for i in range(len(data)):
                    data[i].var["epsilon"] = 1 - data[i].var["phiPart"]
            else:
                print('Volume fraction {} not available. Average procedure skipped.'
                        .format(volume_fraction))
                return

        #Check the size of array "quantity" in order to manage 1D-arrays,
        #vectors and matrices
        size = data[0].var[quantity].shape
        if size[0] != len(data[0].var[volume_fraction]):
            print('{} and {} does not have the same size.'\
                    .format(volume_fraction, quantity) \
                    + ' Average procedure skipped')
            return

        #size[1] provides the number of coordinates, 3 for a vector, 9 for a
        #matrix. For a one dimensional array size[1] does not exists.
        try :
            n = size[1]
        except IndexError:
            n = 1

        volume_fraction_mean = np.zeros(size[0])
        quantity_mean = np.zeros((n, size[0]))
        for i in range(len(data)):
            volume_fraction_mean += data[i].var[volume_fraction]
            if n==1:
                quantity_mean[0] += data[i].var[volume_fraction] \
                        *data[i].var[quantity]
            else:
                for j in range(n):
                    quantity_mean[j] += data[i].var[volume_fraction] \
                        *data[i].var[quantity][:,j]

        nonZeroIndices = np.where(volume_fraction_mean>0)
        for j in range(n):
            quantity_mean[j][nonZeroIndices] /= volume_fraction_mean[nonZeroIndices]
        volume_fraction_mean /= len(data)

        self.__setattr__(volume_fraction+'Mean', volume_fraction_mean)
        if n == 1:
            #Otherwise this is a 1D array inside an array.
            self.__setattr__(quantity+'Mean', quantity_mean[0])
        else:
            self.__setattr__(quantity+'Mean', quantity_mean)
        
        try:
            self.averaged_variables += [quantity+'Mean']
        except AttributeError:
            self.averaged_variables = [quantity+'Mean']
        if volume_fraction+'Mean' not in self.averaged_variables:
            self.averaged_variables += [volume_fraction+'Mean']

    def save_average_fields(self, file_name='average.hdf5', overwrite=0, verbose=True):
        """
        Save interesting average quantites.

        Args:
            file_name : str, name of the file in which the variable will be saved.
                It has to be a .hdf5 file.
            overwrite : 0 or 1, if 1, overwrite already existing saved
                variables in the hdf5 file.
            verbose: bool, set to False to remove output.
                will still show warnings and errors.
        """
        for var in self.loaded_general_variables+self.averaged_variables+self.time_variables:
            self.write_data(var, file_name, overwrite=overwrite, verbose=verbose)
        
    def keys(self, verbose=True):
        if verbose is True:
            print("Loaded general variables are :")
            print(self.loaded_general_variables)
            print("Averaged variables are :")
            try:
                print(self.averaged_variables)
            except AttributeError:
                print('No variables averaged.')
            print("Loaded time variables are :")
            print(self.loaded_time_variables)
        return self.loaded_general_variables, self.averaged_variables, self.loaded_time_variables
    
    def compute_stresses(self):
        '''
        Compute the fluctuating granular kinetic stresses and add them to the 
        contact stresses.
        '''
        
        ####################
        # KINETIC STRESSES #
        ####################
        # sigma_ij^k = -rhoP*phi*<vi'vj'> = rhoP*phi*(<vi><vj> - <vivj>)
        self.kinStressesMean = np.zeros((9,len(self.phiPartMean)))
        self.kinStressesMean[0] = self.densPart*self.phiPartMean*(self.vPartMean[0]*self.vPartMean[0]-self.vivjPartMean[0])
        self.kinStressesMean[1] = self.densPart*self.phiPartMean*(self.vPartMean[0]*self.vPartMean[1]-self.vivjPartMean[1])
        self.kinStressesMean[2] = self.densPart*self.phiPartMean*(self.vPartMean[0]*self.vPartMean[2]-self.vivjPartMean[2])
        self.kinStressesMean[3] = self.densPart*self.phiPartMean*(self.vPartMean[1]*self.vPartMean[0]-self.vivjPartMean[3])
        self.kinStressesMean[4] = self.densPart*self.phiPartMean*(self.vPartMean[1]*self.vPartMean[1]-self.vivjPartMean[4])
        self.kinStressesMean[5] = self.densPart*self.phiPartMean*(self.vPartMean[1]*self.vPartMean[2]-self.vivjPartMean[5])
        self.kinStressesMean[6] = self.densPart*self.phiPartMean*(self.vPartMean[2]*self.vPartMean[0]-self.vivjPartMean[6])
        self.kinStressesMean[7] = self.densPart*self.phiPartMean*(self.vPartMean[2]*self.vPartMean[1]-self.vivjPartMean[7])
        self.kinStressesMean[8] = self.densPart*self.phiPartMean*(self.vPartMean[2]*self.vPartMean[2]-self.vivjPartMean[8])
        
        self.totalStressesMean = self.contactStressesMean + self.kinStressesMean
        
        self.PTot = -(self.totalStressesMean[0]+self.totalStressesMean[4]+self.totalStressesMean[8])/3
        self.shearStressTot = self.totalStressesMean[2]
        
        for name in ["kinStressesMean", "totalStressesMean", "PTot", "shearStressTot"]:
            if name not in self.averaged_variables:
                self.averaged_variables.append(name)
        

    def granular_temperature_balance(self, plot=True):
        '''
        Compute the different terms of the granular temperature equation
        '''
        
        self.compute_stresses()

        ########################
        # GRANULAR TEMPERATURE #
        ########################
        self.T = np.zeros(len(self.phiPartMean))
        index = np.where(self.phiPartMean>0.0)[0]
        self.T[index] = -(self.kinStressesMean[0]+self.kinStressesMean[4]+self.kinStressesMean[8])[index]/(3*self.densPart*self.phiPartMean[index])

        ##################
        # PRODUCION TERM #
        ##################
        self.shearRatePartMean = derivate(self.zAxis, self.vPartMean[0])
        self.Tprod = self.shearStressTot*self.shearRatePartMean

        ##################
        # DIFFUSION TERM #
        ##################

        #Diffusion of kinetic energy (collisional part)
        # Qz = Qzxx, Qzxy, Qzxz
        #      Qzyx, Qzyy, Qzyz
        #      Qzzx, Qzzy, Qzzz
        self.Qzc = - np.array([
            self.diffxMean[2], self.diffyMean[2], self.diffzMean[2],
            self.diffxMean[5], self.diffyMean[5], self.diffzMean[5],
            self.diffxMean[8], self.diffyMean[8], self.diffzMean[8]])
        
        #Diffusion of fluctating kinetic energy (collisional part)
        # qzij = Qzij + sigma_iz <V>j
        self.sigmaV = np.array([
            self.contactStressesMean[2]*self.vPartMean[0], self.contactStressesMean[2]*self.vPartMean[1], self.contactStressesMean[2]*self.vPartMean[2],
            self.contactStressesMean[5]*self.vPartMean[0], self.contactStressesMean[5]*self.vPartMean[1], self.contactStressesMean[5]*self.vPartMean[2],
            self.contactStressesMean[8]*self.vPartMean[0], self.contactStressesMean[8]*self.vPartMean[1], self.contactStressesMean[8]*self.vPartMean[2]])
        self.qzc = self.Qzc + self.sigmaV

        #Diffusion of kinetic energy (fluctuating part)
        # Qzij = 1/2 rhoP phiP <vz'vivj> = 1/2 rhoP phiP ( <vzvivj> - <vz><vivj> )
        self.Qzt = 1./2*self.densPart*self.phiPartMean*(self.vivjvzPartMean-self.vPartMean[2]*self.vivjPartMean)
        #Diffusion of fluctuating kinetic energy (fluctuating part)
        # qzij = Qzij + sigma_iz <V>j
        term = 1./2*self.densPart*self.phiPartMean*np.array([
            -self.vPartMean[0]*self.vivjPartMean[6] - self.vPartMean[0]*self.vivjPartMean[6] + 2*self.vPartMean[2]*self.vPartMean[0]*self.vPartMean[0],
            -self.vPartMean[0]*self.vivjPartMean[7] - self.vPartMean[1]*self.vivjPartMean[6] + 2*self.vPartMean[2]*self.vPartMean[0]*self.vPartMean[1],
            -self.vPartMean[0]*self.vivjPartMean[8] - self.vPartMean[2]*self.vivjPartMean[6] + 2*self.vPartMean[2]*self.vPartMean[0]*self.vPartMean[2],
            -self.vPartMean[1]*self.vivjPartMean[6] - self.vPartMean[0]*self.vivjPartMean[7] + 2*self.vPartMean[2]*self.vPartMean[1]*self.vPartMean[0],
            -self.vPartMean[1]*self.vivjPartMean[7] - self.vPartMean[1]*self.vivjPartMean[7] + 2*self.vPartMean[2]*self.vPartMean[1]*self.vPartMean[1],
            -self.vPartMean[1]*self.vivjPartMean[8] - self.vPartMean[2]*self.vivjPartMean[7] + 2*self.vPartMean[2]*self.vPartMean[1]*self.vPartMean[2],
            -self.vPartMean[2]*self.vivjPartMean[6] - self.vPartMean[0]*self.vivjPartMean[8] + 2*self.vPartMean[2]*self.vPartMean[2]*self.vPartMean[0],
            -self.vPartMean[2]*self.vivjPartMean[7] - self.vPartMean[1]*self.vivjPartMean[8] + 2*self.vPartMean[2]*self.vPartMean[2]*self.vPartMean[1],
            -self.vPartMean[2]*self.vivjPartMean[8] - self.vPartMean[2]*self.vivjPartMean[8] + 2*self.vPartMean[2]*self.vPartMean[2]*self.vPartMean[2]])
        self.qzt = self.Qzt + term

        self.Qz = self.Qzt + self.Qzc
        self.qz = self.qzt + self.qzc

        #In the granular temperature equation, the trace of the matrix
        self.QzTot = self.Qz[0]+self.Qz[4]+self.Qz[8]
        self.qzTot = self.qz[0]+self.qz[4]+self.qz[8]
        self.qzKin = self.qzt[0]+self.qzt[4]+self.qzt[8]
        self.qzCon = self.qzc[0]+self.qzc[4]+self.qzc[8]

        #Diffusion is the gradient of the flux
        self.diffEc = derivate(self.zAxis, self.QzTot)
        self.diffT = -derivate(self.zAxis, self.qzTot)

        #####################
        # DISSIPATION TERMS #
        #####################

        #Dissipation through drag force
        meanDragmeanVel = np.array([
            self.dragForceMean[0]*self.vPartMean[0], self.dragForceMean[0]*self.vPartMean[1], self.dragForceMean[0]*self.vPartMean[2],
            self.dragForceMean[1]*self.vPartMean[0], self.dragForceMean[1]*self.vPartMean[1], self.dragForceMean[1]*self.vPartMean[2],
            self.dragForceMean[2]*self.vPartMean[0], self.dragForceMean[2]*self.vPartMean[1], self.dragForceMean[2]*self.vPartMean[2]])
        self.dissipDragTensor = self.densPart*self.phiPartMean*(self.dragWorkMean-meanDragmeanVel)
        self.dissipDrag = self.dissipDragTensor[0] + self.dissipDragTensor[4] + self.dissipDragTensor[8]

        #Dissipation through collisions
        self.dissipColl = self.dissipMean[0] + self.dissipMean[4] + self.dissipMean[8]
        
        for name in ["T", "diffT", "Tprod", "dissipColl", "dissipDrag", "qzKin", "qzCon"]:
            if name not in self.averaged_variables:
                self.averaged_variables.append(name)
        
        ########
        # PLOT #
        ########
        if plot is True:
            plt.plot(self.Tprod, self.zAxis, label=r'$\tau^p\dot{\gamma}$')
            plt.plot(self.diffT, self.zAxis, label=r'$\partial q / \partial z$')
            plt.plot(self.dissipColl, self.zAxis, label=r'$\Gamma^{coll}$')
            plt.plot(self.dissipDrag, self.zAxis, label=r'$\Gamma^{drag}$')
            plt.plot(-self.diffT-self.dissipColl-self.dissipDrag, self.zAxis, 'k--', 
                     label=r'$-\partial q / \partial z - \Gamma^{coll} - \Gamma^{drag}$')
            plt.grid()
            plt.legend()
            plt.ylabel(r'$z$', rotation=True, horizontalalignment='right')
    
    def kinetic_theory(self):
        '''
        Compute the kinetic theory terms
        '''
        I = np.where(self.phiPartMean>=1e-6)[0]
        self.g0 = np.zeros(len(self.phiPartMean))
        self.eta_law = np.zeros(len(self.phiPartMean))
        self.pressure_law = np.zeros(len(self.phiPartMean))
        self.flux_law = np.zeros(len(self.phiPartMean))
        self.dissip_coll_law = np.zeros(len(self.phiPartMean))
        self.dissip_drag_law = np.zeros(len(self.phiPartMean))
        
        self.g0[I] =  1./(2*self.phiPartMean[I]*(1+self.restitCoef))*(self.PTot[I]/(self.densPart*self.phiPartMean[I]*self.T[I])-1)
        self.pressure_law[I] = (self.PTot[I])/(self.densPart*self.T[I])
        self.eta_law[I] = (self.totalStressesMean[2][I]/self.shearRatePartMean[I])/(self.densPart*self.diameterPart*self.T[I]**(0.5))
        self.flux_law[I] = -(self.qzTot[I]/derivate(self.zAxis, self.T)[I])/(self.densPart*self.diameterPart*self.T[I]**(0.5))
        self.dissip_coll_law[I] = self.dissipColl[I]/(self.densPart/self.diameterPart*self.T[I]**(1.5))
        self.dissip_drag_law[I] = self.dissipDrag[I]/(self.densPart/self.diameterPart*self.T[I]**(1.5))
        
        

        for name in ["g0", "pressure_law", "eta_law", "flux_law", "dissip_coll_law", "dissip_drag_law"]:
            if name not in self.averaged_variables:
                self.averaged_variables.append(name)
        

class HydroMean(YadeMean):
    """
    Load time average data of sediment transport YADE simulation.
    Data must be store in hdf5 format.

    Args:
        simu : str, name of folder containing the desired simulation.
            If None, look for all availables simulations in path.
        path : str, path in which to look for simulation simu.
            If None, consider the current and sub-directories.
        hdf5_file : str, name of the hdf5 file to load
        data_to_load : list of str, names of the variables to load.
            If None, load all available variables.
        verbose: bool, set to False to remove loading information.
            will still show warnings and errors.
    """

    def __init__(self, simu=None, path=None, hdf5_file='average.hdf5',
                 data_to_load=None, verbose=True):
        YadeMean.__init__(self, simu, path, hdf5_file, data_to_load,
                          verbose=verbose)
        
    def garzo_dufty(self):
        self.phi_GD = np.linspace(0,0.63,1000)
        alpha = 0.58 + (3.70-0.58)*np.tanh(self.muPart/0.54)
        self.g0_GD = (2-self.phi_GD)/(2*(1-self.phi_GD)**3) + alpha*self.phi_GD**2/(0.635-self.phi_GD)**(1.5)
        
        # Pressure
        self.F1_GD = self.phi_GD*(1+2*(1+self.restitCoef)*self.phi_GD*self.g0_GD)
        
        # Viscosity
        self.eta_kin = (1-2./5*(1+self.restitCoef)*(1-3*self.restitCoef)*self.phi_GD*self.g0_GD)/ \
            ((1-0.25*(1-self.restitCoef)**2-5./24*(1-self.restitCoef**2))*self.g0_GD)
        self.eta_con = self.eta_kin*(4./5*(1+self.restitCoef)*self.phi_GD*self.g0_GD)
        self.eta_bulk = 384./(25*np.pi)*(1+self.restitCoef)*self.phi_GD**2*self.g0_GD
        self.F2_GD = 5*np.pi**(0.5)/96*(self.eta_kin + self.eta_con + self.eta_bulk)
        
        # Heat transfer
        self.kappa_kin = 2*(1 + 3./5*(1+self.restitCoef)**2*(2*self.restitCoef-1)*self.phi_GD*self.g0_GD)/ \
            ((1-7./16*(1-self.restitCoef))*(1+self.restitCoef)*self.g0_GD)
        self.kappa_con = self.kappa_kin*(6./5*(1+self.restitCoef)*self.phi_GD*self.g0_GD)
        self.kappa_bulk = 2304./(225*np.pi)*(1+self.restitCoef)*self.phi_GD**2*self.g0_GD
        self.F3_GD = 225*np.pi**(0.5)/1152*(self.kappa_kin + self.kappa_con + self.kappa_bulk)
        
        # Contact dissipation
        fmu = 3./2*self.muPart*np.exp(-3*self.muPart)
        eff = self.restitCoef - fmu
        self.F4_GD = -12./np.pi**(0.5)*(1-self.restitCoef**2)*self.phi_GD**2*self.g0_GD
        self.F4_GD *= (1-eff**2)/(1-self.restitCoef**2)
    
