from yadetools._version import __version__
from yadetools.read_hdf5 import TimeData
from yadetools._utils import derivate, integrate, primitive
from yadetools.manage_env import define_yade_simu_path
from yadetools.yadesimu import YadeSimu, YadeFull, YadeMean
from yadetools.hydrosimu import HydroFull, HydroMean
from yadetools.intrusimu import IntruFull, IntruMean

import os
from os.path import join, dirname
import dotenv

#Load environment variable
env_file = join(dirname(__file__), '.env')
try:
    env_file = dotenv.find_dotenv(env_file, raise_error_if_not_found=True)
    dotenv.load_dotenv(env_file)

except OSError:
    pass
