"""Module to manage .env file
=============================
This modules provides functions to define usefull environnement
variables for the yadetools classes.
"""

import dotenv
import os
from os.path import join, dirname

def define_yade_simu_path(simu_path=None):
    """
    Define a python environment variable containing the general
    path of your YADE simulations.

    Args:
        simu_path : str, path to your YADE simulations.
    """

    if simu_path is None:
        simu_path = input("\n Please provide the general path of your YADE simulations. "
            + "Format : /home/user/general/path/to/my/simulations/ : \n")

    #Some manipulations in case the path is not in the exact right format
    if simu_path.startswith('\''):
        simu_path = path[1:]
    if simu_path.endswith('\''):
        simu_path = path[:-1]
    if simu_path.endswith('/') is False:
        simu_path += '/'

    env_file = join(dirname(__file__), '.env')
    try:
        os.environ['YADE_SIMU_PATH']
        dotenv.set_key(env_file, 'YADE_SIMU_PATH', simu_path)
        dotenv.load_dotenv(env_file)
        print("\n The general path of your YADE simulations has been modify"
            + " to {}.".format(os.environ['YADE_SIMU_PATH'])
            + " Restart your python session to account for it.")
    except KeyError:
        dotenv.set_key(env_file, 'YADE_SIMU_PATH', simu_path)
        dotenv.load_dotenv(env_file)
        print("\n The general path of your YADE simulations has been define"
            + " to {}.".format(os.environ['YADE_SIMU_PATH'])
            + " You can later modify it calling function define_yade_simu_path()")

