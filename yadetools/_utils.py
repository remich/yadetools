"""Useful mathematical functions
================================
This module provides functions postprocess data

"""

import numpy as np

def derivate(variable, function):
    """
    Make the derivation of function with respect to variable.
    Centered derivative except on both bounds where right derivative 
    and left derivative are performed. Thus the output has the same 
    length than function.
    variable : array
    function : array
    variable and function have to be of same size
    """

    derivFunc = np.zeros(len(function))
    derivFunc[0] = float(function[1]-function[0])/float(variable[1]-variable[0])
    derivFunc[-1] = float(function[-1]-function[-2])/float(variable[-1]-variable[-2])
    for i in range(1,len(function)-1):
        derivFunc[i] =  float(function[i+1]-function[i-1])/float(variable[i+1]-variable[i-1])

    return derivFunc

def integrate(variable, function, dz=None):
    """
    Integrate function as a function of variable.
    Integration by the method of trapeze
    """
    integral = 0.
    for k in range(len(function)-1):
        integral += (variable[k+1]-variable[k])*(function[k+1]+function[k])/2

    return integral

def primitive(variable, function):
    """
    Compute a primitive of function
    """
    primit = [0.0]
    for i in range(1,len(function)):
        primit.append(integrate(variable[:i],function[:i]))

    return np.array(primit)

