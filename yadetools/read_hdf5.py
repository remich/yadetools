"""Reading YADE results with Python
===================================
This module provides function to read YADE results file in hdf5

"""

import os
import h5py
import numpy as np

class Error(Exception):
    pass

class DirectorySimuError(Error):
    def __init__(self, simu):
        super(DirectorySimuError,self).__init__(
            "No directory found for simulation named {}".format(simu))

def find_directory(path, simu):
    """
    Look for the directory of simu in all the sub directories of path. If several
    directories are found, the program asks which directory is the good one.

    Args:
        path : str, reference path where simulations are stored.
        simu : str, name of the simulation that has to be loaded. If None, it will
            lists all existing simulation names in path and ask you to choose
    """
    directories = []
    sub_directories = [x[0] for x in os.walk(path)]

    for f in sub_directories:
        if f.endswith(simu):
            directories.append(f)

    #If no directories found
    if len(directories) == 0:
        raise DirectorySimuError(simu)

    #If several directories found, ask for the one wanted
    elif len(directories) > 1:
        print("\n The following simulations has been found :")
        for i in range(len(directories)):
            print("{} : {}".format(i, directories[i]))
        chosen_simulation = -1
        while type(chosen_simulation) is not int or (
                chosen_simulation < 0 or chosen_simulation > len(directories) - 1):
            chosen_simulation = int(input(
                "\n Please, choose one simulation ! (integer between {} and {})".format(
                    0,len(directories)-1)
                )
            )
        directory = directories[chosen_simulation]

    else:
        directory = directories[0]

    return directory + '/'

def choose_simulation(path, file_name='data.hdf5'):
    """
    Make a list of all directories located in path containing a simulation.
    Ask the user which simulation to load

    Args:
        path : str, reference path where simulations are stored.
        file_name : str, consider that a directory corresponds to a simulation
            if it contains a file named file_name.
    """
    directories = []
    sub_directories = [x[0] for x in os.walk(path)]

    for f in sub_directories:
        if os.path.isfile(f+'/'+file_name):
            directories.append(f)

    #If no directories found
    if len(directories) == 0:
        raise DirectorySimuError(path)
    
    print("\n The following simulations have been found:")
    for i in range(len(directories)):
        print("{} : {}".format(i, directories[i]))
    chosen_simulation = -1
    while type(chosen_simulation) is not int or (
            chosen_simulation < 0 or chosen_simulation > len(directories) - 1):
        chosen_simulation = int(input(
            "Please, choose one simulation ! (integer between {} and {})".format(
                0,len(directories)-1)
            )
        )
    directory = directories[chosen_simulation]

    return directory + '/'


class TimeData(object):

    def __init__(self, time_step, directory, hdf5_file=None, data_to_load=None):
        """
        Load one time step of the data in a hdf5 file.

        Args:
            time_step : int, time step of the simulation simu to load
            directory : str, directory in which the hdf5 file is located
            hdf5_file : str, name of the hdf5 file to load. If None, take the first one found.
            data_to_load : list of str, name of variables to load. If None, load
                all available variables.
        """
        self.directory = directory
        self.hdf5_file = hdf5_file
        self.time_step = time_step

        self.load_data_dict(data_to_load)

    def load_data_dict(self, data_to_load=None):
        """
        Creates a dictionary (self.var) and stores the data inside. 
        Hdf5 sub-groups are converted into sub-dictionaries
        e.g. 'time_step/a/b/data' -> self.var['a']['b']['data']
        """
        def populate_data_dict(name, value):
            # Private function with :
            #   name : str, name of the hdf5 object (e.g. 'a/b/../data')
            #   value : hdf5 object (Group or Dataset)
            # Find or create a subdictionary of self.var corresponding to the 
            # value subgroup, then populate it with either a numpy array or a
            # dictionnary depending if value is a hdf5 dataset or a hdf5 group.
            if data_to_load is None or name in data_to_load :
                keys = name.split('/')
                dictionary = self.var
                for key in keys[:-1]:
                    dictionary = dictionary.setdefault(key, {})
                if isinstance(value, h5py.Dataset):
                    dictionary[keys[-1]] = np.array(value)
                else:
                    dictionary[keys[-1]] = {}
        self.var = dict()
        with h5py.File(self.directory + self.hdf5_file) as f:
            f[str(self.time_step)].visititems(populate_data_dict)
            self.loaded_variables = list(self.var.keys())
            
    def keys(self, verbose=True):
        """
        Print the name of all variables loaded from data
        """
        if verbose is True:
            print("Loaded available variables are :")
            print(self.loaded_variables)
        return self.loaded_variables
