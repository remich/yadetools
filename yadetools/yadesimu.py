"""Class to load all data saved at timeStep of an openFoam simulation
=====================================================================

.. autoclass:: YadeSimu
.. autoclass:: YadeFull
.. autoclass:: YadeMean

"""
from yadetools import TimeData
from yadetools.read_hdf5 import choose_simulation, find_directory
from yadetools.manage_env import define_yade_simu_path
import h5py
import sys
import os

class YadeSimu(object):
    """
    A general class representing a Yade simulation. Locate the
    directory of a simulation. Data must be stored in hdf5 format.

    Args:
        simu : str, name of folder containing the desired simulation.
            If None, look for all availables simulations in path.
        path : str, path in which to look for simulation simu.
            If None, consider the general path of simulation location.
        hdf5_file : str, name of the hdf5 file to load
    """

    def __init__(self, simu, path, hdf5_file):
        if path is None:
            try:
                path = os.environ["YADE_SIMU_PATH"]
            except KeyError:
                response = input("\n You haven't yet define a general path of your"
                    + " YADE simulations. It allows you not to provide \'path\'"
                    + " argument each time you use such class. Would you like to"
                    + " define it now ? (yes/no)\n")
                if response == "yes":
                    define_yade_simu_path()
                    path = os.environ["YADE_SIMU_PATH"]
                else:
                    path = input("\n Please provide a simulation path.\n")

        self.path = path
        if simu==None:
            self.directory = choose_simulation(path, file_name=hdf5_file)
            self.simu = self.directory.split('/')[-2]
        else:
            self.simu = simu
            self.directory = find_directory(path, simu)

        self.hdf5_file=hdf5_file

    def write_data(self, var_name, file_name, var_value=None, overwrite=0, 
                   verbose=True):
        """
        Save in the .hdf5 file fileName, the variable varName.

        Args:
            var_name : str, name of the variable
            file_name : str, name of the file in which the variable will be saved.
                It has to be a .hdf5 file.
            var_value : array, values of varName to save. If None, look for the
                object variable called varName.
            overwrite : 0 or 1, if 1, overwrite already existing saved
                variables in the hdf5 file.
            verbose: bool, set to False to remove output.
                will still show warnings and errors.
        """
        if var_value is None:
            try:
                var_value = self.__getattribute__(var_name)
            except AttributeError:
                print("Variable {} does not exist.".format(var_name))
                return
        file_name = self.directory+'/'+file_name
        h5_file = h5py.File(file_name, 'a')
        try:
            grp = h5_file.create_dataset(name=var_name, data=var_value)
        except ValueError:
            if overwrite==1:
                del h5_file[var_name]
                grp = h5_file.create_dataset(name=var_name, data=var_value)
                if verbose:
                    print("Variable with name {} has been overwritten".format(var_name))
            else:
                print("Variable with name {} already exists in {}".format(var_name, file_name))
        h5_file.close()


class YadeFull(YadeSimu):
    """
    Load time data of a YADE simulation. Data must be stored in hdf5
    format.

    Args:
        simu : str, name of folder containing the desired simulation.
            If None, look for all availables simulations in path.
        path : str, path in which to look for simulation simu.
            If None, consider the general path of simulation location.
        hdf5_file : str, name of the hdf5 file to load
        start : int, first time step to load
        end : int, last time step to load. If None, load the last
            available time step in hdf5_file
        step : int, step in between each loaded time step. If None,
            load all time step from start to end.
        data_to_load : list of str, names of the variables to load.
            If None, load all available variables.
        verbose: bool, set to False to remove loading information.
            will still show warnings and errors.
    """

    def __init__(self, simu=None, path=None, hdf5_file='data.hdf5',
            start=0, end=None, step=None, data_to_load=None, verbose=True):
        YadeSimu.__init__(self, simu, path, hdf5_file)
        self.load_hdf5(start=start, end=end, step=step,
                       data_to_load=data_to_load, verbose=verbose)

    def load_hdf5(self, start=0, end=None, step=None, data_to_load=None,
                  verbose=True):
        """
        Load time data located in hdf5 file.
        """

        #First, load general parameters of the simulation
        self.loaded_general_variables = []
        load_file = h5py.File(self.directory + self.hdf5_file)
        try :
            grp = load_file['generalParameters']
            for var_name in grp.keys():
                self.__setattr__(var_name, grp[var_name][()])
                self.loaded_general_variables.append(var_name)
            load_file.close()
        except KeyError: #for compatibility with previous script without generalParameters
            pass

        #Second, load the time data
        load_file = h5py.File(self.directory + self.hdf5_file)
        # The keys in hdf5 files are not sorted making difficult to find the first and
        # last time step. Trick to find these values.
        n = list(load_file.keys())
        try:
            n.remove('generalParameters')
        except ValueError: #for compatibility with previous script without generalParameters
            pass
        try:
            n.remove('\x01') #in case last time step is corrupted when hot copies during simulation
        except ValueError:
            pass
        for i in range(len(n)):
            n[i] = int(n[i])
        n.sort()
        first_time_step = int(n[0])
        last_time_step = int(n[-1])
        load_file.close()

        if start < first_time_step:
            print("Warning : start={} is smaller than the first time step recorded" \
                    + "in the hdf5 file. start is set to the first time step".format(
                        start))
            start = first_time_step

        if end == None:
            end = last_time_step
        elif end > last_time_step:
            print("Warning : end={} is bigger than the last time step recorded in" \
                    + "the hdf5 file. end is set to the last time step.".format(end))
            end = last_time_step

        if step == None:
            step = 1

        self.data = []
        if verbose:
            print("\n Start to load simulation {}.".format(self.directory))
        for i in range(start, end + 1, step):
            if verbose:
                sys.stdout.write('Loading time step {}\r'.format(i)),
                sys.stdout.flush()
            try:
                self.data.append(TimeData(i, directory=self.directory,
                                          hdf5_file=self.hdf5_file, 
                                          data_to_load=data_to_load))
            except KeyError:
                print(f'Time step {i} corrupted, not loaded')
        
        self.loaded_time_variables = self.data[0].loaded_variables

    def keys(self, verbose=True):
        if verbose is True:
            print("Loaded general variables are :")
            print(self.loaded_general_variables)
            print("Loaded time variables are :")
            print(self.loaded_time_variables)
        return self.loaded_general_variables, self.loaded_time_variables


class YadeMean(YadeSimu):
    """
    Load time average data of a YADE simulation. Data must be store in
    hdf5 format.

    Args:
        simu : str, name of folder containing the desired simulation.
            If None, look for all availables simulations in path.
        path : str, path in which to look for simulation simu.
            If None, consider the general path of simulation location.
        hdf5_file : str, name of the hdf5 file to load
        data_to_load : list of str, names of the variables to load.
            If None, load all available variables.
        verbose: bool, set to False to remove loading information.
            will still show warnings and errors.
    """

    def __init__(self, simu=None, path=None, hdf5_file='average.hdf5',
                 data_to_load=None, verbose=True):
        YadeSimu.__init__(self, simu, path, hdf5_file)
        self.load_hdf5(data_to_load=data_to_load, verbose=verbose)

    def load_hdf5(self, data_to_load=None, verbose=True):
        """
        Load hdf5 file.
        """
        load_file = h5py.File(self.directory + self.hdf5_file)
        if data_to_load is None:
            data_to_load = load_file.keys()
        self.loaded_variables = []
        for var_name in load_file.keys():
            if var_name in data_to_load:
                self.__setattr__(var_name, load_file[var_name][()])
                if verbose:
                    print("Variable {} has been loaded.".format(var_name))
                self.loaded_variables.append(var_name)
        load_file.close()

    def keys(self, verbose=True):
        """
        Print the name of all variables loaded from data
        """
        if verbose is True:
            print("Loaded available variables are :")
            print(self.loaded_variables)
        return self.loaded_variables
