# yadetools

The yadetools module provides python classes to post-process results of YADE
simulations.

# Info on this repository

- YADE postprocesssing tools
- Version 0.0.1
- Supported Python Versions : >= 3.7

# Installation instructions

First, clone the repository into your locale device:
```
git clone https://gitlab.com/remich/yadetools.git
```
Enter the directory and install required python dependencies and the yadetools package:
```
make install-requirements
make install
```
You are now ready to use the yadetools module with some simple examples located into the `examples/` directory.

# Core developer

- Rémi Chassagne : remi.chassagne@univ-grenoble-alpes.fr

# License

yadetools is distributed under the GNU General Public License v2 (GPLv2).
